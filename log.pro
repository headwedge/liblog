QT += core
QT -= gui

CONFIG += c++11

TARGET = log
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    test_log.cpp \
    active_log.cpp

HEADERS += \
    mpmc_bounded_queue.h \
    scheduler.h \
    active_object.h \
    active_log.h \
    active_ostream.h \
    active_log_ini.h \
    libclk/jittery_delay.h \
    libclk/stopwatch.h

DISTFILES += \
    README.md
